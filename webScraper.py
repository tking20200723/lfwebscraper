# -*- coding: utf-8 -*-

# Written in Python 3.7.4
# Author: Tony King
# Created 26/07/2020

# Purpose:  to scrape job listings from the url https://jobs.sanctuary-group.co.uk/search
#           writing the distinct details of job title, care home, location, department, operation,
#           requisition number, salary, closing date and job description to a .csv file.

# Import modules
import os
import re       # Regex library
import csv
from time import sleep
from urllib.request import urlopen

### Functions ###
def getItem(pattern, searchIn, desc):

    # Use regex to extract content
    regexResult = re.findall(pattern, searchIn)

    # If no match found return message
    if regexResult == []:
        return desc + ' not found'

    res = regexResult[0].strip()
    if res == '':
        return desc + ' not found'

    # Return actual value
    return res


def getJobDetails(jobURL):
    # Read job details page
    jobDetailsPg = urlopen(jobURL, data=None).read().decode()

    jobDetailsPg = jobDetailsPg.replace('&ndash;','-')
    
    # Get job title
    jobTitle = getItem('itemprop="title">([a-zA-Z, \s0-9-]+)</span>',jobDetailsPg,'Job title')

    # Get location
    location = getItem('jobGeoLocation">([a-zA-Z, \s0-9-]+)</span>',jobDetailsPg,'Location')

    # Get care home name
    careHome = getItem('he team at ([a-zA-Z ,0-9-]+)is',jobDetailsPg,'Care home')
    town = location[:location.find(',')]
    if careHome == 'Care home not found':
        careHome = getItem('he team at ([a-zA-Z 0-9-]*).',jobDetailsPg,'Care home')
    if careHome == 'Care home not found':
        careHome = getItem('<strong>([a-zA-Z ,0-9-]+)[, ]*'+town,jobDetailsPg,'Care home')
    if careHome == 'Care home not found':
        careHome = getItem('>([a-zA-Z ,0-9-]+)[, ]*'+town,jobDetailsPg,'Care home')
    if careHome.find(',') != -1:
        careHome = careHome[:careHome.find(',')]

    # Get department
    department = getItem('itemprop="department">([a-zA-Z, \s()0-9-]+)</span>',jobDetailsPg,'Department')
    if department == 'Department not found':
        department = getItem('itemprop="dept">([a-zA-Z, \s()0-9-]+)</span>',jobDetailsPg,'Department')

    # Get operation
    operation = getItem('itemprop="facility">([a-zA-Z, \s0-9-]+)</span>',jobDetailsPg,'Operation')

    # Get reqNo
    reqNo = getItem('itemprop="customfield5">([a-zA-Z, \s0-9-]+)</span>',jobDetailsPg,'Requisition No.')

    # Get salary
    salary = getItem('&pound;[a-zA-Z, .&;\s0-9-]+',jobDetailsPg,'Salary')
    salary = salary.replace('&pound;', "£")

    # Get closng date
    htmlClosingDate = getItem('Closing [a-zA-Z]+:([a-zA-Z, &;0-9-]+)</s',jobDetailsPg,'Closing date')
    if htmlClosingDate == 'Closing date not found':
        htmlClosingDate = getItem('>([a-zA-Z, &;0-9-]+2020)</s',jobDetailsPg,'Closing date')
    if htmlClosingDate == 'Closing date not found':
        htmlClosingDate = getItem('Closing [a-zA-Z]+:</strong>[\s]*<strong>([a-zA-Z, &;0-9-]+)</strong>',jobDetailsPg,'Closing date')
    if htmlClosingDate == 'Closing date not found':
        htmlClosingDate = getItem('Closing [a-zA-Z]+:</strong>&nbsp;<strong>([a-zA-Z, &;0-9-]+)</strong>',jobDetailsPg,'Closing date')
    if htmlClosingDate == 'Closing date not found':
        htmlClosingDate = getItem('ate+:([a-zA-Z 0-9-]+)</s',jobDetailsPg,'Closing date')
    closingDate = htmlClosingDate.replace('&nbsp;',' ').strip()

    # Extract complete job details element block, <div class="jobDisplay">, including line breaks
    pattern = re.compile('<div class="jobDisplay">(.*)<aside id="similar-jobs"',re.DOTALL)
    jobElementBlock = pattern.search(jobDetailsPg).group(0)
    
    # Remove distinct information
    jobDescContent = re.sub(jobTitle,'',jobElementBlock)
    jobDescContent = re.sub(location,'',jobDescContent)
    jobDescContent = re.sub(careHome,'',jobDescContent)
    jobDescContent = re.sub(operation,'',jobDescContent)
    
    jobDescContent = re.sub(reqNo,'',jobDescContent)
    jobDescContent = re.sub(salary,'',jobDescContent)
    jobDescContent = re.sub(htmlClosingDate,'',jobDescContent)

    # Strip out non-breaking spaces
    jobDescContent = re.sub('&nbsp;',' ',jobDescContent)

    # Get element contents    
    elementContentList = re.findall('>([a-zA-Z0-9,.&;: #-]+)<',jobDescContent)

    # Pull element contents into single string
    jobDescription = ''.join(elementContentList)

    # Return list containing data items
    return [jobTitle, location, careHome, department, operation, reqNo, salary, closingDate, jobDescription]

### End of functions ###

### Main code ###

# Global variables
baseURL = 'https://jobs.sanctuary-group.co.uk'
searchIndexURL = baseURL + '/search/'
searchQuery = '?q=&sortColumn=referencedate&sortDirection=desc&startrow='

# Initialise counters
jobsPerPage = 25
startRow = 0
jobCount = 0

# Flag for the 1st page
firstPage = True

# List/array object for holding all job details
jobListings = []

# Read initial page content - Job index
indexPage = urlopen(searchIndexURL, data=None).read().decode()

# Extract element containing 'Results <b>1 – 25</b> of <b>171</b>' and get total job count
paginationBlock = re.search('<span class="paginationLabel"[a-zA-Z0-9" =><\/–-]+</span>',indexPage).group(0)
totalJobs = int(re.findall('<b>([0-9]*)</b>',paginationBlock)[0])

# Loop until all job details obtained
while jobCount < totalJobs:

    # Check if we're still on the 1st page
    if firstPage:
        firstPage = False
    else: # Get next page
        searchURL = searchIndexURL + searchQuery + str(startRow)
        indexPage = urlopen(searchURL, data=None).read().decode()

    # Extract all job links (will include 2 of each)
    jobLinks = []
    jobLinks = re.findall('(/job/[a-zA-Z0-9/-]+)"',indexPage)

    # Loop through job links
    lastLink = ''
    for link in jobLinks:

        # Check we're not looking at a duplicate
        if lastLink != link:
            # Build list of jobs - using getJobDetails function
            jobListings.append(getJobDetails(baseURL + link))
            print(link)  # Output job link, to track progress

            # Increment job counter, for main loop
            jobCount += 1

        # Remember link for next pass
        lastLink = link
        sleep(.2) # Pause between pages

    # Increment query row count
    startRow += jobsPerPage

# file name for output
csvOutput = "job-listing.csv"

# Open a new file for writing too
with open(csvOutput, 'w') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL, escapechar='\\', quotechar='"')

    headers = ['jobTitle', 'location', 'careHome', 'department', 'operation', 'reqNo',
           'salary', 'closingDate', 'jobDescription']

    writer.writerow(headers)   # Write data to CSV
    for job in jobListings:
        writer.writerow(job)





